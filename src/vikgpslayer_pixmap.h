/*
 * viking -- GPS Data and Topo Analyzer, Explorer, and Manager
 *
 * Copyright (C) 2003-2005, Evan Battaglia <gtoevan@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* GdkPixbuf RGB C-Source image dump 1-byte-run-length-encoded */

static const GdkPixdata gpslayer_pixbuf = {
  0x47646b50, /* Pixbuf magic: 'GdkP' */
  24 + 116, /* header length + pixel_data length */
  0x2010001, /* pixdata_type */
  48, /* rowstride */
  16, /* width */
  16, /* height */
  /* pixel_data: */
  "\235\377\377\377\1\377\0\0\215\377\377\377\203\377\0\0\213\377\377\377"
  "\203\377\0\0\213\377\377\377\203\377\0\0\213\377\377\377\202\377\0\0"
  "\215\377\377\377\204\377\0\0\216\377\377\377\204\377\0\0\216\377\377"
  "\377\204\377\0\0\214\377\377\377\202\377\0\0\214\377\377\377\203\377"
  "\0\0\214\377\377\377\202\377\0\0\215\377\377\377\1\377\0\0\215\377\377"
  "\377\202\377\0\0\216\377\377\377\1\377\0\0\235\377\377\377",
};


