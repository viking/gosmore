/* gosmore - European weed widely naturalized in North America having 
yellow
   flower heads and leaves resembling a cat's ears */
   
/* This software is placed by in the public domain by its authors. */
/* Written by Nic Roets with contribution(s) from Dave Hansen. */

/* 
Dual core with 1GB RAM  : real   8m12.369s user 11m7.727s sys 0m15.199s
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <limits.h>

/******** SHARED ********/

#define BUCKETS (1<<22) /* Must be power of 2 */
#define TO_HALFSEG -1
#define TILEBITS (18)

#define stricmp strcasecmp

typedef struct {
  int lon, lat, other, wayPtr;
} halfSegType;

typedef struct {
  int type : 6;
  int layer : 3;
  int oneway : 1;
  int zoom16384 : 17; /* To make the way just fill the display */
  int name; /* Offset into pak file */
  int clat, clon; /* Centre */
} wayType;

enum { motorway, motorway_link, trunk, primary, secondary, tertiary,
  unclassified, residential, service, track, footway, rail, river, stream,
  canel, city, town, station, suburb, village, hamlet, junction, place,
  unsupportedWayType, unwayed
};


inline int Hash (int lon, int lat)
{ /* This is a universal hashfuntion in GF(2^31-1). The hexadecimal numbers */
  /* are from random.org, but experimentation will surely yield better */
  /* numbers. The more we right shift lon and lat, the larger */
  /* each tile will be. We can add constants to the lat and lon variables */
  /* to make them positive, but the clashes will still occur between the */
  /* same tiles. */

  /* Mercator projection means tiles are not the same physical size. */
  /* Compensating for this very low on the agenda vs. e.g. compensating for */
  /* high node density in Western Europe. */
  long long v = ((lon >> TILEBITS) /* + (1<<19) */) * (long long) 0x00d20381 +
    ((lat >> TILEBITS) /*+ (1<<19)*/) * (long long) 0x75d087d9;
  while (v >> 31) v = (v & ((1<<31) - 1)) + (v >> 31);
  /* Replace loop with v = v % ((1<<31)-1) ? */
  return v & (BUCKETS - 1);
} /* This mask means the last bucket is very, very slightly under used. */


int Latitude (double lat)
{ /* Mercator projection onto a square means we have to clip
     everything beyond N85.05 and S85.05 */
  return lat > 85.051128779 ? 2147483647 : lat < -85.051128779 ? -2147483647 :
    lrint (log (tan (M_PI / 4 + lat * M_PI / 360)) / M_PI * 2147483648.0);
}

int Longitude (double lon)
{
  return lrint (lon / 180 * 2147483648.0);
}

/******** REBUILD *********/

#define MAX_NODES 2814112
#define MAX_SEGMENTS 2814112
#define MAX_WAYS 2800000
#define MAX_NAMED_NODES 140000

typedef struct {
  int id, lon, lat;
} nodeType;

typedef struct {
  wayType w;
  char *name;
  int idx;
} wayBuildType;


typedef struct {
  char *feature, *name;
} highwayType;

/* TODO: make sure same better */
/* this must be mirrored for drawing with correct color types */
const highwayType highway[] = {
  /* ways */
  { "highway", "motorway"      },
  { "highway", "motorway_link" },
  { "highway", "trunk"         },
  { "highway", "primary"       },
  { "highway", "secondary"     },
  { "highway", "tertiary"      },
  { "highway", "unclassified"  },
  { "highway", "residential"   },
  { "highway", "service"       },
  { "highway", "track"         },
  { "highway", "footway"       },
  { "railway", "rail"          },
  { "waterway", "river"        },
  { "waterway", "stream"       },
  { "waterway", "canal"        },
  /* nodes : */
  { "place",   "city"          },
  { "place",   "town"          },
  { "railway", "station"       },
  { "place",   "suburb"        },
  { "place",   "village"       },
  { "place",   "halmet"        },
  { "place",   "junction"      },
  { NULL, NULL /* named node of unidentified type i.e. place */ },
  { NULL, NULL /* unsupportedWayType */ },
  { NULL, NULL /* unwayed */ }
};


void quicksort (void *base, int n, int size,
  int (*cmp)(const void *, const void*))
{ /* Builtin qsort performs badly when dataset does not fit into memory,
     probably because it uses mergesort. quicksort quickly divides the
     problem into sections that is small enough to fit into memory and
     finishes them before tackling the rest. */
  static halfSegType pivot[2]; /* 1 segment is largest object we're sorting */
  
  if (size * n > 50000000) printf ("%9d items of %d bytes\n", n, size);
  char *l = (char*) base, *h = (char*) base + (n - 1) * size;
  memcpy (pivot, l + n / 2 * size, size);
  memcpy (l + n / 2 * size, l, size);
  while (l < h) {
    while (l < h && (*cmp)(pivot, h) <= 0) h -= size;
    if (l < h) memcpy (l, h, size);
    while (l < h && (*cmp)(pivot, l) >= 0) l += size;
    if (l < h) memcpy (h, l, size);
  }
  if (l > h) fprintf (stderr, "sort warning !\n");
  memcpy (l, pivot, size);
  if (l - (char*) base > size)
    quicksort (base, (l - (char*) base) / size, size, cmp);
  if ((n - 2) * size > h - (char*) base)
    quicksort (h + size, n - (h - (char*) base) / size - 1, size, cmp);
}

int WayBuildCmp (const void *a, const void *b)
{
  return !((wayBuildType *)a)->name ? -1 : !((wayBuildType *)b)->name ? 1 :
    strcmp (((wayBuildType *)a)->name, ((wayBuildType *)b)->name);
}

inline nodeType *FindNode (nodeType *table, int id)
{
  unsigned hash = id;
  for (;;) {
    nodeType *n = &table[hash % MAX_NODES];
    if (n->id < 0 || n->id == id) return n;
    hash = hash * (long long) 1664525 + 1013904223;
  }
}

inline halfSegType *FindSegment (halfSegType *table, int id)
{
  unsigned hash = id;
  for (;;) {
    halfSegType *s = &table[(hash % MAX_SEGMENTS) * 2];
    if (s->wayPtr < 0 || s->other == id) return s;
    hash = hash * (long long) 1664525 + 1013904223;
  }
}

int HalfSegCmp (const halfSegType *a, const halfSegType *b)
{
  int hasha = Hash (a->lon, a->lat), hashb = Hash (b->lon, b->lat);
  return hasha != hashb ? hasha - hashb : a->lon != b->lon ? a->lon - b->lon :
    a->lat - b->lat;
}

int build ( const char *dest_fn )
{
  FILE *pak;
  int i, strPtr, bucket;

  {
    if (!(pak = fopen (dest_fn, "w+"))) {
      fprintf (stderr, "Cannot create %s\n", dest_fn);
      return 0;
    }
    // Set up something for unwayed segments to point to. This will be
    // written to location 0 when we encounter the first <way> tag.
  
    char tag[301], key[301], value[301], quote, feature[301];
    int wayCnt = 0, segId;
    enum { doNodes, doSegments, doWays } mode = doNodes;
    int wleft, wright, wtop, wbottom;

    printf ("Reading nodes...\n");
    nodeType *node = (nodeType *) malloc (sizeof (*node) * MAX_NODES), *n;
    halfSegType *halfSeg = (halfSegType *) malloc (sizeof (*halfSeg) *
      (MAX_SEGMENTS + MAX_NAMED_NODES) * 2);
    halfSegType *namedNodeHs = halfSeg + 2 * MAX_SEGMENTS;
    wayBuildType *w = (wayBuildType *) calloc (sizeof (*w), MAX_WAYS);
    if (!node || !halfSeg || !w) {
      fprintf (stderr, "Out of memory. It may work if MAX_SEGMENTS and / or\n"
        "MAX_NODES are reduced\n");
      return 3;
    }
    memset (node, -1, sizeof (*node) * MAX_NODES);
    memset (halfSeg, -1, sizeof (*halfSeg) * MAX_SEGMENTS * 2);
/* Initially this array is a hashtable with 2 adjacent entries for each
   segment and the segment id stored in 'other'.
   
   After processing the ways the named nodes are added and the blanks are
   removed. 'other' then is the index into the array before it is sorted.
   Then we sort by bucket number, lon and lat. Then we set 'other'
   to the offset of the other half in the sorted pak file. */
    w[wayCnt].idx = wayCnt;
    w[wayCnt].w.type = unwayed;
    w[wayCnt].w.layer = 5; // 5 means show duplicated segments clearly.
    w[wayCnt].name = strdup ("_unwayed");
    assert (unwayed == sizeof (highway) / sizeof (highway[0]) - 1);
   
    while (scanf (" <%300[a-zA-Z0-9?/]", tag) == 1) {
      //printf ("%s", tag);
      do {
        while (scanf (" %300[a-zA-Z0-9]", key)) {
          if (getchar () == '=') {
            quote = getchar ();
            if (quote == '\'') scanf ("%300[^']'", value); /* " */
            else if (quote == '"') scanf ("%300[^\"]\"", value); /* " */
            else {
              ungetc (quote, stdin);
              scanf ("%300[^ ]", value); /* " */
            }
            //printf (" %s='%s'", key, value);
            if (mode == doWays && !stricmp (tag, "seg") &&
                !stricmp (key, "id")) {
              halfSegType *hs = FindSegment (halfSeg, atoi (value));
              if (hs->wayPtr >= 0) {
                hs->wayPtr = wayCnt;
                for (i = 0; i < 2; i++) {
                  if (wleft > hs[i].lon) wleft = hs[i].lon;
                  else if (wright < hs[i].lon) wright = hs[i].lon;
                  if (wbottom > hs[i].lat) wbottom = hs[i].lat;
                  else if (wtop < hs[i].lat) wtop = hs[i].lat;
                }
                w[wayCnt].w.clon = wleft / 2 + wright / 2; /* eager evaluat */
                w[wayCnt].w.clat = wtop / 2 + wbottom / 2;
                w[wayCnt].w.zoom16384 = (wright - wleft > wtop - wbottom ?
                  wright - wleft : wtop - wbottom) >> 14;
              }
            }
            else if (!stricmp (tag, "tag") /* && mode != doSegments but
            then we will have hundreds of complains of tagged segments */) {
              if (!stricmp (key, "k")) strcpy (feature, value);
              if (mode != doSegments && !stricmp (key, "v")) {
                if (!stricmp (feature, "oneway") &&
                  tolower (value[0]) == 'y') w[wayCnt].w.oneway = 1;
                else if (!strcmp (feature, "layer"))
                  w[wayCnt].w.layer = atoi (value);
                else if (!strcmp (feature, "name")) {
                  w[wayCnt].name = strdup (value);
                  if (mode == doNodes) namedNodeHs += 2;
                }
                //else if (!strcmp (feature, "ref")) strcpy (ref, value);
                else for (i = 0; highway[i].name; i++) {
		  if (!stricmp (highway[i].feature, feature) &&
                      !stricmp (value, highway[i].name)) w[wayCnt].w.type = i;
                }
              }
            }
            else if (strcmp (tag, "?xml") && strcmp (tag, "osm")) {
              /* First flush out a way in progress */
              if (mode == doWays && !w[wayCnt].name) {
                w[wayCnt].name = strdup ("");
              }
              if (w[wayCnt].name) wayCnt++;
              mode = doNodes;
              if (!stricmp (tag, "node")) {
                if (!stricmp (key, "id")) {
                  n = FindNode (node, atoi (value));
                  n->id = atoi (value);
                }
                if (!stricmp (key, "lat")) n->lat = Latitude (atof (value));
                if (!stricmp (key, "lon")) {
                  n->lon = Longitude (atof (value));
                  
                  if (w[wayCnt].name) wayCnt++;
                  /* Now there is a way for the unwayed segment plus
                     wayCnt - 1 nodes with names, each with 2 half segments */
                  w[wayCnt].w.type = place; /* generic */
                  w[wayCnt].idx = wayCnt;
                  w[wayCnt].w.clat = n->lat;
                  w[wayCnt].w.clon = n->lon;
                  w[wayCnt].w.zoom16384 = 10;
                  /* We prepare a way and two segments in case this node has
                     a name. */
                  /* namedNodeHs->other = 0; */
                  namedNodeHs->lat = w[wayCnt].w.clat;
                  namedNodeHs->lon = w[wayCnt].w.clon;
                  namedNodeHs->wayPtr = wayCnt;
                  memcpy (namedNodeHs+1, namedNodeHs, sizeof (*namedNodeHs));
                  
                  //if (nodeCnt % 100000 == 0) printf ("%9d nodes\n", nodeCnt);
                }
              }
              else if (!stricmp (tag, "segment")) {
                mode = doSegments;
                if (!stricmp (key, "id")) segId = atoi (value);
                if (!stricmp (key, "from")) n = FindNode (node, atoi (value));
                if (!stricmp (key, "to") && n->id != -1) {
                  nodeType *to = FindNode (node, atoi (value));
                  if (to->id != -1) {
                    halfSegType *seg = FindSegment (halfSeg, segId);
                    seg[0].other = segId;
                    seg[0].wayPtr = 0;
                    seg[0].lon = n->lon;
                    seg[0].lat = n->lat;
                    /* seg[1].other = ; */
                    seg[1].wayPtr = TO_HALFSEG;
                    seg[1].lon = to->lon;
                    seg[1].lat = to->lat;
                  }
                }
              }
              else if (!stricmp (tag, "way")) {
                mode = doWays;
                w[wayCnt].idx = wayCnt;
                w[wayCnt].w.type = unsupportedWayType;
                wleft = INT_MAX;
                wright = -INT_MAX;
                wbottom = INT_MAX;
                wtop = -INT_MAX;
              }
              else fprintf (stderr, "Unexpected tag %s\n", tag);
            } /* If we expected a node, a segment or a way */
          } /* if key / value pair found */
        } /* while search for key / value pairs */
      } while (getchar () != '>');
      //printf ("\n");
    } /* while we found another tag */
    if (mode == doWays && !w[wayCnt].name) {
      w[wayCnt].name = strdup ("");
    }
    if (w[wayCnt].name) wayCnt++; /* Flush the last way */
    free (node);
    printf ("Sorting ways by name\n");
    qsort (w, wayCnt, sizeof (*w), WayBuildCmp);
    int *wIdx = (int *) malloc (sizeof (*wIdx) * wayCnt);
    printf ("Writing ways\n");

    for (i = 0, strPtr = wayCnt * sizeof (w[0].w); i < wayCnt; i++) {
      w[i].w.name = strPtr;
      fwrite (&w[i].w, sizeof (w[i].w), 1, pak);
      strPtr += strlen (w[i].name) + 1;
      wIdx[w[i].idx] = i * sizeof (w[0].w); // = ftell (pak)
    }
    for (i = 0; i < wayCnt; i++) {
      fwrite (w[i].name, strlen (w[i].name) + 1, 1, pak);
      free (w[i].name);
    }
    free (w);
    printf ("Preparing for sorting half segments\n");
    int halfSegCnt;
    for (halfSegCnt = 0; halfSeg + halfSegCnt < namedNodeHs; ) {
      if (namedNodeHs[-2].wayPtr == -1) namedNodeHs -= 2;
      else {
        if (halfSeg[halfSegCnt].wayPtr == -1) {
          memcpy (&halfSeg[halfSegCnt], namedNodeHs - 2,
            sizeof (*halfSeg) * 2);
          namedNodeHs -= 2;
        }
        halfSeg[halfSegCnt].other = halfSegCnt;
        halfSeg[halfSegCnt + 1].other = halfSegCnt + 1;
        halfSegCnt += 2;
      }
    }
    printf ("Sorting\n");
    quicksort (halfSeg, halfSegCnt, sizeof (*halfSeg),
	       (int (*)(const void*, const void*)) HalfSegCmp);
	       
    printf ("Calculating addresses\n");
    int *hsIdx = (int *) malloc (sizeof (*hsIdx) * halfSegCnt);
    int hsBase = ftell (pak);
    if (hsBase & 15) hsBase += fwrite (&hsBase, 1, 16 - (hsBase & 15), pak);
    /* Align to 16 bytes */
    for (i = halfSegCnt - 1; i >= 0; i--)
	       hsIdx[halfSeg[i].other] = hsBase + i * sizeof (*halfSeg);
    printf ("Writing Data\n");
    for (i = 0; i < halfSegCnt; i++) {
      halfSeg[i].other = hsIdx[halfSeg[i].other ^ 1];
      halfSeg[i].wayPtr = halfSeg[i].wayPtr == TO_HALFSEG ? TO_HALFSEG :
        wIdx[halfSeg[i].wayPtr]; // Final pos of way.
      fwrite (&halfSeg[i], sizeof (*halfSeg), 1, pak);
    }
    printf ("Writing hash table\n");
    fwrite (&hsBase, sizeof (hsBase), 1, pak);
    for (bucket = 0, i = 0; bucket < BUCKETS; bucket++) {
      while (i < halfSegCnt && Hash (halfSeg[i].lon,
				     halfSeg[i].lat) == bucket) {
        i++;
        hsBase += sizeof (*halfSeg);
      }
      fwrite (&hsBase, sizeof (hsBase), 1, pak);
    }
    fclose (pak); /* fflush instead ? */
    free (wIdx);
    free (halfSeg);
    free (hsIdx);
    /* It has BUCKETS + 1 entries so that we can easily look up where each
       bucket begins and ends */
  } /* if rebuilding */
  return 1;
}

int main()
{
  build("gosmore.pak");
  return 0;
}
