#!/usr/bin/perl
$x = "";
$i = 1;

while (<STDIN>) {
  if ( /\<way/ ) {
    $ways = $_;
    last;
  } else {
    print;
  }
}


while (<STDIN>) {
  if (/\<nd ref=['"](.*)['"]/) {
    if ( $x ) {
      print "<segment id='$i' from='$x' to='$1'>\n";
      $ways .= "<seg id='$i' />\n";
      $i++;
    }

    $x = $1;
  } else {
    $ways .= $_;
    if ( /<\/way/ ) {
      $x = "";
    }
  }
}

print $ways;
