/*
 * viking -- GPS Data and Topo Analyzer, Explorer, and Manager
 *
 * Copyright (C) 2003-2005, Evan Battaglia <gtoevan@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
/* GdkPixbuf RGB C-Source image dump 1-byte-run-length-encoded */
 
static const GdkPixdata georeflayer_pixbuf = {
  0x47646b50, /* Pixbuf magic: 'GdkP' */
  24 + 579, /* header length + pixel_data length */
  0x2010001, /* pixdata_type */
  48, /* rowstride */
  16, /* width */
  16, /* height */
  /* pixel_data: */
  "\2\377\377\377*\203&\207\37|\32\11b\243_\377\377\377\377@@\377\217\217"
  "\262\262\262\21\21\21JJJ\377\377\3774\210/\207\37|\32\12\273\327\271"
  "\377\377\377\377@@\24566\6\6\6\214\214\214\376\376\376\377\377\377\256"
  "\317\254>\216:\205\37|\32\6\40}\33\353\363\352\377\377\377\373<<v\6\6"
  "\261\261\261\202\377\377\377\6\344\344\344\377\377\377\374\375\373\343"
  "\357\342\304\334\3028\2123\202\37|\32\5\201\265~\377\377\377\225\225"
  "\225\277\0\0\360\200\200\203\377\377\377\15\0\0\0\21\21\21OOO\343\343"
  "\343\377\377\377\354\364\353\253\316\251\315\342\314\376\376\376\266"
  "\266\266\17\17\17\305\6\6\377\217\217\203\377\377\377\15yyyXXX\24\24"
  "\24\6\6\6WWW\336\336\336\351\351\351\245\245\245NNN\3\3\3\202\202\202"
  "\376\77\77\377\217\217\205\377\377\377\13\373\373\373\301\301\301GGG"
  "\7\7\7\0\0\0\12\12\12CCC\262\262\262\377\377\377\377@@\377\217\217\202"
  "\377\377\377\1\355\357\372\203\377\377\377\5\362\362\362444\33\33\33"
  "\274\274\274\367\367\367\203\377\377\377\5\377@@\377\217\217\331\336"
  "\365|\217\335/L\311\203\377\377\377\3\221\221\221\0\0\0\317\317\317\204"
  "\377\377\377\4\375\375\376\37247\231=z-K\311\202*H\310\202\377\377\377"
  "\3\376\376\376CCC<<<\204\377\377\377\4\344\350\370E`\317\327\16&|,{\203"
  "*H\310\202\377\317\317\3\220aa3\3\3\320\220\220\204\377\277\277\5\221"
  "a\243\217&i\376\0\1\362\4\14t/\202\202m2\212\215\377\0\0\1\351\10\25"
  "\202\344\11\31\4\377\377\377\266\266\266\0\0\0\252\252\252\203\377\377"
  "\377\7\350\353\3711N\312*H\310F>\255\355\6\21\271\30B-G\305\202*H\310"
  "\4\377\377\377ggg\35\35\35\360\360\360\203\377\377\377\1\237\255\346"
  "\203*H\310\2\327\16&z-}\203*H\310\3\377\377\377\35\35\35\203\203\203"
  "\204\377\377\377\1z\215\335\203*H\310\2\327\16&z-}\203*H\310\3\334\334"
  "\334\2\2\2\320\320\320\204\377\377\377\1\220\237\342\203*H\310\2\327"
  "\16&z-}\203*H\310",
};


