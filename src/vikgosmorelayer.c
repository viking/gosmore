/*
 * viking -- GPS Data and Topo Analyzer, Explorer, and Manager
 *
 * Copyright (C) 2003-2005, Evan Battaglia <gtoevan@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <math.h>

#include "vikgosmorelayer.h"
#include "viklayer.h"
#include "vikgosmorelayer_pixmap.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <gtk/gtk.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>

static void gosmore_layer_marshall( VikGosmoreLayer *vgl, guint8 **data, gint *len );
static VikGosmoreLayer *gosmore_layer_unmarshall( guint8 *data, gint len, VikViewport *vvp );
static gboolean gosmore_layer_set_param ( VikGosmoreLayer *vgl, guint16 id, VikLayerParamData data, VikViewport *vp );
static VikLayerParamData gosmore_layer_get_param ( VikGosmoreLayer *vgl, guint16 id );
static VikGosmoreLayer *vik_gosmore_layer_new ( VikViewport *vp );
static void vik_gosmore_layer_free ( VikGosmoreLayer *vgl );
static void vik_gosmore_layer_draw ( VikGosmoreLayer *vgl, gpointer data );


static VikLayerParam gosmore_layer_params[] = {
};


enum { NUM_PARAMS=0 };

VikLayerInterface vik_gosmore_layer_interface = {
  "Gosmore",
  &gosmorelayer_pixbuf,

  NULL,
  0,

//  gosmore_layer_params,
  NULL,
  NUM_PARAMS,
  NULL,
  0,

  VIK_MENU_ITEM_ALL,

  (VikLayerFuncCreate)                  vik_gosmore_layer_new,
  (VikLayerFuncRealize)                 NULL,
                                        NULL,
  (VikLayerFuncFree)                    vik_gosmore_layer_free,

  (VikLayerFuncProperties)              NULL,
  (VikLayerFuncDraw)                    vik_gosmore_layer_draw,
  (VikLayerFuncChangeCoordMode)         NULL,

  (VikLayerFuncSetMenuItemsSelection)   NULL,
  (VikLayerFuncGetMenuItemsSelection)   NULL,

  (VikLayerFuncAddMenuItems)            NULL,
  (VikLayerFuncSublayerAddMenuItems)    NULL,

  (VikLayerFuncSublayerRenameRequest)   NULL,
  (VikLayerFuncSublayerToggleVisible)   NULL,

  (VikLayerFuncMarshall)		gosmore_layer_marshall,
  (VikLayerFuncUnmarshall)		gosmore_layer_unmarshall,

  (VikLayerFuncSetParam)                gosmore_layer_set_param,
  (VikLayerFuncGetParam)                gosmore_layer_get_param,

  (VikLayerFuncReadFileData)            NULL,
  (VikLayerFuncWriteFileData)           NULL,

  (VikLayerFuncDeleteItem)              NULL,
  (VikLayerFuncCopyItem)                NULL,
  (VikLayerFuncPasteItem)               NULL,
  (VikLayerFuncFreeCopiedItem)          NULL,
  (VikLayerFuncDragDropRequest)		NULL,
};

struct _VikGosmoreLayer {
  VikLayer vl;
};

GType vik_gosmore_layer_get_type ()
{
  static GType vgl_type = 0;

  if (!vgl_type)
  {
    static const GTypeInfo vgl_info =
    {
      sizeof (VikGosmoreLayerClass),
      NULL, /* base_init */
      NULL, /* base_finalize */
      NULL, /* class init */
      NULL, /* class_finalize */
      NULL, /* class_data */
      sizeof (VikGosmoreLayer),
      0,
      NULL /* instance init */
    };
    vgl_type = g_type_register_static ( VIK_LAYER_TYPE, "VikGosmoreLayer", &vgl_info, 0 );
  }

  return vgl_type;
}

static void gosmore_layer_marshall( VikGosmoreLayer *vgl, guint8 **data, gint *len )
{
  vik_layer_marshall_params ( VIK_LAYER(vgl), data, len );
}

static VikGosmoreLayer *gosmore_layer_unmarshall( guint8 *data, gint len, VikViewport *vvp )
{
  VikGosmoreLayer *rv = vik_gosmore_layer_new ( vvp );
  vik_layer_unmarshall_params ( VIK_LAYER(rv), data, len, vvp );
  return rv;
}

gboolean gosmore_layer_set_param ( VikGosmoreLayer *vgl, guint16 id, VikLayerParamData data, VikViewport *vp )
{
  switch ( id )
  {

  }
  return TRUE;
}

static VikLayerParamData gosmore_layer_get_param ( VikGosmoreLayer *vgl, guint16 id )
{
  VikLayerParamData rv;
  switch ( id )
  {
//    case PARAM_COLOR: rv.s = vgl->color ? vgl->color : ""; break;
  }
  return rv;
}

gint gosmore_expose (gpointer vp, gdouble glat, gdouble flon, gint zoom );
int gosmore_init();


static void vik_gosmore_layer_draw ( VikGosmoreLayer *vgl, gpointer data )
{
  VikViewport *vp = (VikViewport *) data;
  struct LatLon ll;
  vik_coord_to_latlon ( vik_viewport_get_center(vp), &ll );
  gint zoom = vik_viewport_get_width(vp)*vik_viewport_get_xmpp(vp)*128;
  gosmore_expose ( vp, ll.lat, ll.lon, zoom );
}

static void vik_gosmore_layer_free ( VikGosmoreLayer *vgl )
{
}

static VikGosmoreLayer *vik_gosmore_layer_new ( VikViewport *vp )
{
  VikGosmoreLayer *vgl = VIK_GOSMORE_LAYER ( g_object_new ( VIK_GOSMORE_LAYER_TYPE, NULL ) );
  vik_layer_init ( VIK_LAYER(vgl), VIK_LAYER_GOSMORE );
  gosmore_init();
  return vgl;
}

/* gosmore - European weed widely naturalized in North America having yellow
   flower heads and leaves resembling a cat's ears */
   
/* This software is placed by in the public domain by its authors. */
/* Written by Nic Roets with contribution(s) from Dave Hansen. */

/* 
Dual core with 1GB RAM  : real   8m12.369s user 11m7.727s sys 0m15.199s
*/


#define stricmp strcasecmp

#define BUCKETS (1<<22) /* Must be power of 2 */
#define TILEBITS (18)
#define TILESIZE (1<<TILEBITS)

#define MAX_NODES 2814112
#define MAX_SEGMENTS 2814112
#define MAX_WAYS 280000
#define MAX_NAMED_NODES 14000

inline int Hash (int lon, int lat)
{ /* This is a universal hashfuntion in GF(2^31-1). The hexadecimal numbers */
  /* are from random.org, but experimentation will surely yield better */
  /* numbers. The more we right shift lon and lat, the larger */
  /* each tile will be. We can add constants to the lat and lon variables */
  /* to make them positive, but the clashes will still occur between the */
  /* same tiles. */

  /* Mercator projection means tiles are not the same physical size. */
  /* Compensating for this very low on the agenda vs. e.g. compensating for */
  /* high node density in Western Europe. */
  long long v = ((lon >> TILEBITS) /* + (1<<19) */) * (long long) 0x00d20381 +
    ((lat >> TILEBITS) /*+ (1<<19)*/) * (long long) 0x75d087d9;
  while (v >> 31) v = (v & ((1<<31) - 1)) + (v >> 31);
  /* Replace loop with v = v % ((1<<31)-1) ? */
  return v & (BUCKETS - 1);
} /* This mask means the last bucket is very, very slightly under used. */

typedef struct {
  int id, lon, lat;
} nodeType;

#define TO_HALFSEG -1

typedef struct {
  int lon, lat, other, wayPtr;
} halfSegType;

typedef struct {
  int type : 6;
  int layer : 3;
  int oneway : 1;
  int zoom16384 : 17; /* To make the way just fill the display */
  int name; /* Offset into pak file */
  int clat, clon; /* Centre */
} wayType ;

typedef struct {
  wayType w;
  char *name;
  int idx;
} wayBuildType;

enum { motorway, motorway_link, trunk, primary, secondary, tertiary,
  unclassified, residential, service, track, footway, rail, river, stream,
  canel, city, town, station, suburb, village, hamlet, junction, place,
  unsupportedWayType, unwayed
};

struct highwayType {
  char *feature, *name, *colour;
  int width;
  double invSpeed; /* 1.0 is the fastest. Everything else must be bigger. */
  GdkLineStyle style;
} highway[] = {
  /* ways */
  { "highway", "motorway"     , "blue",   3, 1.0, GDK_LINE_SOLID },
  { "highway", "motorway_link", "blue",   3, 1.0, GDK_LINE_SOLID },
  { "highway", "trunk"        , "green",  3, 120.0 / 70.0, GDK_LINE_SOLID },
  { "highway", "primary"      , "red",    2, 120.0 / 60.0, GDK_LINE_SOLID },
  { "highway", "secondary"    , "orange", 2, 120.0 / 50.0, GDK_LINE_SOLID },
  { "highway", "tertiary"     , "yellow", 1, 120.0 / 40.0, GDK_LINE_SOLID },
  { "highway", "unclassified" , "darkgrey", 1, 120.0 / 40.0, GDK_LINE_SOLID },
  { "highway", "residential"  , "black",  1, 120.0 / 34.0, GDK_LINE_SOLID },
  { "highway", "service"      , "darkgrey", 1, 120.0 / 40.0, GDK_LINE_SOLID },
  { "highway", "track"        , "brown",  1, 120.0 / 30.0, GDK_LINE_SOLID },
  { "highway", "footway"      , "brown",  1, 120.0 / 5.0, GDK_LINE_SOLID },
  { "railway", "rail"         , "black",  3, 99.0, GDK_LINE_ON_OFF_DASH },
  { "waterway", "river"       , "blue",   2, 120.0 / 5.0, GDK_LINE_SOLID },
  { "waterway", "stream"      , "brown",  1, 120.0 / 5.0, GDK_LINE_SOLID },
  { "waterway", "canal"       , "brown",  1, 120.0 / 5.0, GDK_LINE_SOLID },
  /* nodes : */
  { "place",   "city"         , "black",  3, 1.0, GDK_LINE_SOLID },
  { "place",   "town"         , "black",  2, 1.0, GDK_LINE_SOLID },
  { "railway", "station"      , "red",    1, 1.0, GDK_LINE_SOLID },
  { "place",   "suburb"       , "black",  2, 1.0, GDK_LINE_SOLID },
  { "place",   "village"      , "black",  1, 1.0, GDK_LINE_SOLID },
  { "place",   "halmet"       , "black",  1, 1.0, GDK_LINE_SOLID },
  { "place",   "junction"     , "black",  1, 1.0, GDK_LINE_SOLID },
  { NULL, NULL /* named node of unidentified type i.e. place */  , "gray",
    1, 1.0, GDK_LINE_SOLID },
  { NULL, NULL /* unsupportedWayType */, "gray",   1, 99.0, GDK_LINE_SOLID },
  { NULL, NULL /* unwayed */  , "gray",   1, 99.0, GDK_LINE_SOLID }
};

inline nodeType *FindNode (nodeType *table, int id)
{
  unsigned hash = id;
  for (;;) {
    nodeType *n = &table[hash % MAX_NODES];
    if (n->id < 0 || n->id == id) return n;
    hash = hash * (long long) 1664525 + 1013904223;
  }
}

inline halfSegType *FindSegment (halfSegType *table, int id)
{
  unsigned hash = id;
  for (;;) {
    halfSegType *s = &table[(hash % MAX_SEGMENTS) * 2];
    if (s->wayPtr < 0 || s->other == id) return s;
    hash = hash * (long long) 1664525 + 1013904223;
  }
}


void quicksort (void *base, int n, int size,
  int (*cmp)(const void *, const void*))
{ /* Builtin qsort performs badly when dataset does not fit into memory,
     probably because it uses mergesort. quicksort quickly divides the
     problem into sections that is small enough to fit into memory and
     finishes them before tackling the rest. */
  static halfSegType pivot[2]; /* 1 segment is largest object we're sorting */
  
  if (size * n > 50000000) printf ("%9d items of %d bytes\n", n, size);
  char *l = (char*) base, *h = (char*) base + (n - 1) * size;
  memcpy (pivot, l + n / 2 * size, size);
  memcpy (l + n / 2 * size, l, size);
  while (l < h) {
    while (l < h && (*cmp)(pivot, h) <= 0) h -= size;
    if (l < h) memcpy (l, h, size);
    while (l < h && (*cmp)(pivot, l) >= 0) l += size;
    if (l < h) memcpy (h, l, size);
  }
  if (l > h) fprintf (stderr, "sort warning !\n");
  memcpy (l, pivot, size);
  if (l - (char*) base > size)
    quicksort (base, (l - (char*) base) / size, size, cmp);
  if ((n - 2) * size > h - (char*) base)
    quicksort (h + size, n - (h - (char*) base) / size - 1, size, cmp);
}

int Latitude (double lat)
{ /* Mercator projection onto a square means we have to clip
     everything beyond N85.05 and S85.05 */
  return lat > 85.051128779 ? 2147483647 : lat < -85.051128779 ? -2147483647 :
    lrint (log (tan (M_PI / 4 + lat * M_PI / 360)) / M_PI * 2147483648.0);
}

int Longitude (double lon)
{
  return lrint (lon / 180 * 2147483648.0);
}

/*---------- Global variables -----------*/
int *hashTable;
char *data;

typedef struct { // Iterate over all the objects in a square
  halfSegType *hs[2]; /* Readonly. Either can be 'from' or 'to', but you */
  /* can be guaranteed that nodes will be in hs[0] */
  
  int slat, slon, left, right, top, bottom; /* Private */
  halfSegType *end;
  
} OsmItr;

  void osm_itr_init (OsmItr *itr, int l, int t, int r, int b)
  {
    itr->left = l & (~(TILESIZE - 1));
    itr->right = (r + TILESIZE - 1) & (~(TILESIZE-1));
    itr->top = t & (~(TILESIZE - 1));
    itr->bottom = (b + TILESIZE - 1) & (~(TILESIZE-1));
    
    itr->slat = itr->top;
    itr->slon = itr->left - TILESIZE;
    itr->hs[0] = itr->end = NULL;
  }

int Next (OsmItr *itr) /* Friend of osmItr */
{
  do {
    itr->hs[0]++;
    while (itr->hs[0] >= itr->end) {
      if ((itr->slon += TILESIZE) == itr->right) {
        itr->slon = itr->left;  /* Here we wrap around from N85 to S85 ! */
        if ((itr->slat += TILESIZE) == itr->bottom) return FALSE;
      }
      int bucket = Hash (itr->slon, itr->slat);
      itr->hs[0] = (halfSegType *) (data + hashTable[bucket]);
      itr->end = (halfSegType *) (data + hashTable[bucket + 1]);
    }
  } while (((itr->hs[0]->lon ^ itr->slon) >> TILEBITS) ||
           ((itr->hs[0]->lat ^ itr->slat) >> TILEBITS) ||
      ((itr->hs[1] = (halfSegType *) (data + itr->hs[0]->other)) > itr->hs[0] &&
       itr->left <= itr->hs[1]->lon && itr->hs[1]->lon < itr->right &&
       itr->top <= itr->hs[1]->lat && itr->hs[1]->lat < itr->bottom));
/* while hs[0] is a hash collision, or is the other half of something that
has already or will soon be iterated over. Test doesn't work for wrapping. */
  return TRUE;
}

typedef struct _routeNodeType routeNodeType;
struct _routeNodeType {
  halfSegType *hs;
  routeNodeType *shortest;
  int best, heapIdx;
};

routeNodeType *route = NULL, *shortest = NULL, **routeHeap;
int dhashSize, routeHeapSize, limit, tlat, tlon, flat, flon, car, fastest;

#define Sqr(x) ((x)*(x))
int Best (routeNodeType *n)
{
  return limit < 2000000000 ? n->best : n->best +
    lrint (sqrt (Sqr ((long long)(n->hs->lon - flon)) +
                 Sqr ((long long)(n->hs->lat - flat))));
}

#define ZOOM_PAD_SIZE 20
#define STATUS_BAR    0

GtkWidget *draw, *carBtn, *fastestBtn;
int clon, clat, zoom;
/* zoom is the amount that fits into the window (regardless of window size) */


typedef struct { // Build a list of names, sort by name,
  wayType *w;            // make unique by name, sort by y, then render
  int x, y, width;       // only if their y's does not overlap
} name2renderType;

int Name2RenderNameCmp (const void *a, const void *b)
{
  return strcmp (((name2renderType *)a)->w->name + data,
    ((name2renderType *)b)->w->name + data);
}

int Name2RenderYCmp (const void *a, const void *b)
{
  return ((name2renderType *)a)->y - ((name2renderType *)b)->y;
}

gint gosmore_expose (gpointer vp, gdouble flat, gdouble flon, gint zoom )
{
  GdkColor highwayColour[sizeof (highway) / sizeof (highway[0])];

  gint height = vik_viewport_get_height ( vp );
  gint width = vik_viewport_get_width ( vp );

  /* window to be used ONLY to allocate colors */
  GdkWindow *window = GTK_WIDGET(vp)->window;
  GdkGC *gc = vik_viewport_new_gc(vp, "red", 1);

  gint clat = Latitude(flat);
  gint clon = Longitude(flon);
  int i;

  g_debug ("lat=%d lon=%d zoom=%d\n", clat, clon, zoom);

  for (i = 0; i < sizeof (highway) / sizeof (highway[0]); i++) {
    gdk_color_parse (highway[i].colour, &highwayColour[i]);
    gdk_colormap_alloc_color (gdk_window_get_colormap (window),
      &highwayColour[i], FALSE, TRUE); /* Possibly only at startup ? */
  }

  GdkRectangle clip;
  clip.x = 0;
  clip.y = 0;
  clip.height = height;
  clip.width = width;
/*
  gdk_gc_set_clip_rectangle (gc, &clip);
  gdk_gc_set_foreground (gc, &highwayColour[0]);
  gdk_draw_line (draw->window, gc,
    clip.width - ZOOM_PAD_SIZE / 2, 0,
    clip.width - ZOOM_PAD_SIZE / 2, clip.height); // Visual queue for zoom bar
  gdk_draw_line (draw->window, gc,
    clip.width - ZOOM_PAD_SIZE, clip.height - ZOOM_PAD_SIZE / 2,
    clip.width - ZOOM_PAD_SIZE / 2, clip.height); // Visual queue for zoom bar
  gdk_draw_line (draw->window, gc,
    clip.width, clip.height - ZOOM_PAD_SIZE / 2,
    clip.width - ZOOM_PAD_SIZE / 2, clip.height); // Visual queue for zoom bar
  gdk_draw_line (draw->window, gc,
    clip.width - ZOOM_PAD_SIZE, ZOOM_PAD_SIZE / 2,
    clip.width - ZOOM_PAD_SIZE / 2, 0); // Visual queue for zoom bar
  gdk_draw_line (draw->window, gc,
    clip.width, ZOOM_PAD_SIZE / 2,
    clip.width - ZOOM_PAD_SIZE / 2, 0); // Visual queue for zoom bar
*/
    
  clip.width = width;// - ZOOM_PAD_SIZE;
  gdk_gc_set_clip_rectangle (gc, &clip);
  
  GdkFont *f = gtk_style_get_font (GTK_WIDGET(vp)->style);
  name2renderType name[3000];
  int perpixel = zoom / clip.width, nameCnt = 0;
//    zoom / sqrt (draw->allocation.width * draw->allocation.height);
  int thisLayer, nextLayer;
  for (thisLayer = -5; thisLayer < 6; thisLayer = nextLayer) {


    OsmItr itr;
    osm_itr_init (&itr, clon - perpixel * clip.width, clat - perpixel * clip.height,
      clon + perpixel * clip.width, clat + perpixel * clip.height);
    // Widen this a bit so that we render nodes that are just a bit offscreen ?
    nextLayer = 6;


    while (Next (&itr)) {
/***** BEGIN MYSTERY BLOCK ****/
      wayType *w = (wayType *)(data + (itr.hs[0]->wayPtr != TO_HALFSEG ?
        itr.hs[0]->wayPtr : itr.hs[1]->wayPtr));
      if (thisLayer < w->layer && w->layer < nextLayer) nextLayer = w->layer;
      if (w->layer != thisLayer) continue;
/***** END MYSTERY BLOCK ****/

      if (nameCnt < sizeof (name) / sizeof (name[0]) &&
            data[w->name] != '\0') {
        gint lbearing, rbearing, width, ascent, descent;
        gdk_string_extents (f, w->name + data, &lbearing, &rbearing, &width,
          &ascent, &descent);

        name[nameCnt].width = itr.hs[1]->lon - itr.hs[0]->lon;
        if (name[nameCnt].width < 0) name[nameCnt].width *= -1; /* abs () */
        name[nameCnt].x = (itr.hs[0]->lon / 2 + itr.hs[1]->lon / 2 - clon) /
          perpixel + clip.width / 2 - width / 2;
        name[nameCnt].y = f->descent + clip.height / 2 -
          (itr.hs[0]->lat / 2 + itr.hs[1]->lat / 2 - clat) / perpixel;
        if (-f->ascent < name[nameCnt].y &&
            name[nameCnt].y < clip.height + f->descent &&
            -width / 2 < name[nameCnt].x &&
            name[nameCnt].x < clip.width + width / 2) name[nameCnt++].w = w;
      }


      if (w->type < city || w->type == unwayed) {
        gdk_gc_set_foreground (gc,
          &highwayColour[w->type]);
        gdk_gc_set_line_attributes (gc,
          highway[w->type].width, highway[w->type].style, GDK_CAP_PROJECTING,
          GDK_JOIN_MITER);
        vik_viewport_draw_line ( vp, gc,
          (itr.hs[0]->lon - clon) / perpixel + clip.width / 2,
          clip.height / 2 - (itr.hs[0]->lat - clat) / perpixel,
          (itr.hs[1]->lon - clon) / perpixel + clip.width / 2,
          clip.height / 2 - (itr.hs[1]->lat - clat) / perpixel);
      }
    } /* for each visible tile */
  }

  qsort (name, nameCnt, sizeof (name[0]), Name2RenderNameCmp);
  int deleted;
  for (i = 1, deleted = 0; i < nameCnt; ) {
    memcpy (name + i, name + i + deleted, sizeof (name[0]));
    // I guess memcpy will always work (do nothing) if deleted == 0
    if (i && !strcmp (name[i - 1].w->name + data, name[i].w->name + data)) {
      if (name[i - 1].width < name[i].width) 
        memcpy (name + i - 1, name + i + deleted, sizeof (name[0]));
      deleted++; // Keep only coordinates with larger 'width'
      nameCnt--;
    }
    else i++;
  }
  qsort (name, nameCnt, sizeof (name[0]), Name2RenderYCmp);
//  printf ("%d %d %s\n", name[0].x, name[0].y, name[0].w->name + data);
  int y;
  for (i = 0, y = -1000; i < nameCnt; i++) {
    if (y + f->ascent + f->descent < name[i].y) {
      y = name[i].y;
      gdk_gc_set_foreground (gc,
        &highwayColour[name[i].w->type]);
      vik_viewport_draw_string (vp, f, gc,
        name[i].x, name[i].y, name[i].w->name + data);
    }
  }

#if 0
  gdk_gc_set_foreground (gc, &highwayColour[rail]);
  gdk_gc_set_line_attributes (gc,
    1, GDK_LINE_SOLID, GDK_CAP_PROJECTING, GDK_JOIN_MITER);
  routeNodeType *x;
  if (shortest && (x  = shortest->shortest)) {
    vik_viewport_draw_line ( vp, gc,
      (gint)((flon - clon) / perpixel + clip.width / 2),
      clip.height / 2 - (flat - clat) / perpixel,
      (x->hs->lon - clon) / perpixel + clip.width / 2,
      clip.height / 2 - (x->hs->lat - clat) / perpixel);
        g_debug("draw line");
    for (; x->shortest; x = x->shortest) {
      vik_viewport_draw_line ( vp, gc,
        (x->hs->lon - clon) / perpixel + clip.width / 2,
        clip.height / 2 - (x->hs->lat - clat) / perpixel,
        (x->shortest->hs->lon - clon) / perpixel + clip.width / 2,
        clip.height / 2 - (x->shortest->hs->lat - clat) / perpixel);
    }
    vik_viewport_draw_line ( vp, gc,
      (x->hs->lon - clon) / perpixel + clip.width / 2,
      clip.height / 2 - (x->hs->lat - clat) / perpixel,
      (tlon - clon) / perpixel + clip.width / 2,
      clip.height / 2 - (tlat - clat) / perpixel);
  }
#endif

  return FALSE;
}

GtkWidget *search;
GtkWidget *list;
wayType *incrementalWay;
#define wayArray ((wayType *)data)
#define wayCount (wayArray[0].name / sizeof (wayArray[0]))

int gosmore_init()
{
  FILE *pak;
  GMappedFile *gmap = g_mapped_file_new ("gosmore/gosmore.pak", FALSE, NULL);
  if (!gmap) {
    fprintf (stderr, "Cannot read gosmore.pak\nYou can (re)build it from\n"
      "the planet file e.g. bzip2 -d planet-...osm.bz2 | %s rebuild\n",
      "gosmore");
    return 4;
  }
  data = (char*) g_mapped_file_get_contents (gmap);
  hashTable = (int *) (data + g_mapped_file_get_length (gmap)) - BUCKETS - 1;

  wayType *w = 0 + (wayType *) data;
  //printf ("%d ways %d\n", w[0].name / sizeof (w[0]), w[w[0].name / sizeof (w[0]) - 1].name);
  clon = Longitude (28.30803);
  clat = Latitude (-25.78569);
  zoom = (w[0].zoom16384 + 3) << 14;
    //lrint (0.1 / 180 * 2147483648.0 * cos (26.1 / 180 * M_PI));
}

