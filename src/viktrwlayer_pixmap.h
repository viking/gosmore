/*
 * viking -- GPS Data and Topo Analyzer, Explorer, and Manager
 *
 * Copyright (C) 2003-2005, Evan Battaglia <gtoevan@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* GdkPixbuf RGBA C-Source image dump 1-byte-run-length-encoded */

static const GdkPixdata trwlayer_pixbuf = {
  0x47646b50, /* Pixbuf magic: 'GdkP' */
  24 + 514, /* header length + pixel_data length */
  0x2010002, /* pixdata_type */
  64, /* rowstride */
  16, /* width */
  16, /* height */
  /* pixel_data: */
  "\3\\\\\\\377\0\0\0\377\202\202\202\377\205\377\377\377\377\2\365\324"
  "\324\377\363\312\312\377\206\377\377\377\377\4\366\366\366\377GGG\377"
  "\15\15\15\377\357\357\357\377\204\377\377\377\377\3\342yy\377\314\23"
  "\23\377\375\370\370\377\206\377\377\377\377\4\374\374\374\377TTT\377"
  "^^^\377\375\375\375\377\203\377\377\377\377\2\373\355\355\377\364\317"
  "\317\377\210\377\377\377\377\4\361\361\361\377%%%\377OOO\377\354\354"
  "\354\377\215\377\377\377\377\4\326\326\326\377\32\32\32\377666\377\365"
  "\365\365\377\215\377\377\377\377\3\332\332\332\377\35\35\35\377TTT\377"
  "\202\377\377\377\377\2\342||\377\346\215\215\377\203\377\377\377\377"
  "\2\376\376\376\377\177\177\177\377\205\377\377\377\377\6\303\303\303"
  "\377\6\6\6\377\315\315\315\377\377\377\377\377\356\263\263\377\352\237"
  "\237\377\202\377\377\377\377\3\307\307\307\377888\377\36\36\36\377\206"
  "\377\377\377\377\3RRR\377333\377\366\366\366\377\202\377\377\377\377"
  "\5\356\356\356\377kkk\377\2\2\2\377RRR\377\355\355\355\377\206\377\377"
  "\377\377\10\307\307\307\377\3\3\3\377;;;\377\341\341\341\377|||\377\20"
  "\20\20\377\12\12\12\377\232\232\232\377\211\377\377\377\377\6\210\210"
  "\210\377\0\0\0\377\1\1\1\377\0\0\0\377///\377\325\325\325\377\211\377"
  "\377\377\377\6\261\261\261\377***\377\0\0\0\377\1\1\1\377nnn\377\370"
  "\370\370\377\210\377\377\377\377\6\375\375\375\377qqq\377\1\1\1\377\0"
  "\0\0\377\25\25\25\377\263\263\263\377\212\377\377\377\377\5uuu\377\3"
  "\3\3\377\0\0\0\377EEE\377\347\347\347\377\212\377\377\377\377\5\226\226"
  "\226\377\3\3\3\377\6\6\6\377\211\211\211\377\375\375\375\377\212\377"
  "\377\377\377\4\257\257\257\377\4\4\4\377&&&\377\313\313\313\377\213\377"
  "\377\377\377\4\365\365\365\377\14\14\14\377]]]\377\362\362\362\377\213"
  "\377\377\377\377",
};
